const assert = require('chai').assert;

const app = require('../index');

activeModulesResult = app.__get__('activeModules');
getCustomModuleNumberResult = app.__get__('getCustomModuleNumber');

describe('App', function() {
	it('activeModules should be type array', function() {
		assert.typeOf(activeModulesResult, 'array');
	});

	it('getCustomModuleNumber should be type number', function() {
		assert.typeOf(getCustomModuleNumberResult, 'number');
	});
})

